import unicorn, ucelf, functools, sys, json

x = ucelf.UTypes(unicorn.UC_ARCH_ARM64, unicorn.UC_MODE_LITTLE_ENDIAN)
x.load_elf(open('/home/sergey/pmos//samsung-kernel/vmlinux', 'rb').read())

y = ucelf.UTypes(unicorn.UC_ARCH_ARM64, unicorn.UC_MODE_LITTLE_ENDIAN)
y.load_elf(open('/home/sergey/home_backup/sergey/linux-3.18.91/vmlinux', 'rb').read())

remap_base = 2**36

remaps = {}

def ARG(uc, which):
    return uc.reg_read(getattr(unicorn.arm64_const, 'UC_ARM64_REG_X'+str(which)))

def RET(uc, value=None):
    if value is not None:
        uc.reg_write(unicorn.arm64_const.UC_ARM64_REG_X0, value)
    uc.reg_write(unicorn.arm64_const.UC_ARM64_REG_PC, uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_LR))

mmio_state = {}

def mmio_read(uc, offset, size, q):
    global last_mmio, last_mmio_times, wait_mmio
    print(q+':', 'read', size, 'bytes at', hex(offset), 'from', hex(uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_PC))+',', 'lr', hex(uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_LR)))
    #if q == 'phys@0x148d0000' and size == 4 and offset == 0x604:
    #    return 0x1000
    #if q == 'phys@0x144d0000' and size == 4 and offset == 0x600:
    #    return 0x1000
    #if q == 'phys@0x13730000' and size == 4 and offset == 0x600:
    #    return 0x1000
    return mmio_state.get((q, offset, size), 0)

def mmio_write(uc, offset, size, value, q):
    print(q+':', 'write', hex(value), 'as', size, 'bytes at', hex(offset), 'from', hex(uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_PC))+',', 'lr', hex(uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_LR)))
    mmio_state[(q, offset, size)] = value

def do_ioremap(uc, what, *, addr=None, size=2**32):
    global remap_base
    if addr is not None:
        ans = addr
    else:
        ans = remap_base
        remap_base += size
    remaps[ans] = what
    uc.mmio_map(ans, size, mmio_read, what, mmio_write, what)
    return ans

def emulate_ioremap(uc, what):
    RET(uc, do_ioremap(uc, what))

def emulate_malloc(uc, size):
    ans = x.scratchpad
    ans += (-ans) % 16
    x.scratchpad = ans + size
    RET(uc, ans)

def emu_of_iomap(uc, *args):
    emulate_ioremap(uc, 'of_iomap(0x%x)'%uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_X0))

def emu___ioremap(uc, *args):
    emulate_ioremap(uc, 'phys@0x%x'%uc.reg_read(unicorn.arm64_const.UC_ARM64_REG_X0))

def emu___kmalloc(uc, *args):
    emulate_malloc(uc, ARG(uc, 0))

def emu_kfree(uc, *args):
    RET(uc)

def emu_kmem_cache_alloc_trace(uc, *args):
    emulate_malloc(uc, ARG(uc, 2))

def emu__raw_spin_lock_irqsave(uc, *args):
    RET(uc)

def emu__raw_spin_unlock_irqrestore(uc, *args):
    RET(uc)

def emu_vm_area_add_early(uc, *args):
    RET(uc)

def emu_map_vm_area(uc, *args):
    RET(uc, 0)

def emu_exynos_smc_readsfr(uc, *args):
    print('smc_readsfr', hex(ARG(uc, 0)), 'lr', hex(ARG(uc, 30)))
    RET(uc, 0)

# XXX: actually important?

#def emu_ect_init_map_io(uc, *args):
#    RET(uc)

#def emu_asv_set_freq_limit(uc, *args):
#    RET(uc)

#def emu_cal_asv_init(uc, *args):
#    RET(uc, 0)

#def emu_ect_get_block(uc, *args):
#    emulate_ioremap(uc, 'ect_block@'+x.string(ARG(uc, 0)).decode('ascii'))

# here mostly stubs

def emu_vmalloc_to_page(uc, *args):
    RET(uc, 0)

def emu_printk(uc, *args):
    print(x.string(ARG(uc, 0)), ARG(uc, 1), ARG(uc, 2), ARG(uc, 3))
    RET(uc, 0)

def emu_of_find_matching_node_and_match(uc, *args):
    global of_alloc
    if ARG(uc, 0) == 0 and ARG(uc, 1) in x.dlsym('ext_clk_match'):
        uc.mem_write(ARG(uc, 2), ARG(uc, 1).to_bytes(8, 'little'))
        return RET(uc, 1235)
    if ARG(uc, 0) == 1235 and ARG(uc, 1) in x.dlsym('ext_clk_match'):
        return RET(uc, 0)
    print('unknown of_find_matching_node_and_match', ARG(uc, 0), hex(ARG(uc, 1)), x.dlsym(ARG(uc, 1)))
    RET(uc, 0)

def emu_of_property_read_u32_array(uc, *args):
    if ARG(uc, 0) == 1235 and x.string(ARG(uc, 1)) == b'clock-frequency':
        uc.mem_write(ARG(uc, 2), 26000000 .to_bytes(4, 'little'))
        return RET(uc, 0)
    print('unknown of_property_read_u32_array', ARG(uc, 0), x.string(ARG(uc, 1)))
    RET(uc, -22)

clock_data = [None]

def emu_clk_register_fixed_rate(uc, *args):
    idx = len(clock_data)
    clock_data.append(('fixed', x.string(ARG(uc, 1)), x.string(ARG(uc, 2)), ARG(uc, 3), ARG(uc, 4)))
    RET(uc, idx)

def emu_clk_register_fixed_factor(uc, *args):
    idx = len(clock_data)
    clock_data.append(('fixed_factor', x.string(ARG(uc, 1)), x.string(ARG(uc, 2)), ARG(uc, 3), ARG(uc, 4), ARG(uc, 5)))
    RET(uc, idx)

def emu_clk_register(uc, *args):
    idx = len(clock_data)
    clock_data.append(('custom', ARG(uc, 0), ARG(uc, 1), x.uc.mem_read(int.from_bytes(x.uc.mem_read(ARG(uc, 1)+8, 8), 'little'), 40)[::-1].hex()))
    RET(uc, idx)

def emu_clk_register_clkdev(uc, *args):
    print('clk_register_clkdev', ARG(uc, 0), x.string(ARG(uc, 1)), x.string(ARG(uc, 2)), ARG(uc, 3), ARG(uc, 4), ARG(uc, 5), ARG(uc, 6))
    RET(uc, 0) # XXX: stub

clk_providers = []

def emu_of_clk_add_provider(uc, *args):
    clk_providers.append((ARG(uc, 0), ARG(uc, 1), ARG(uc, 2)))
    RET(uc, 0)

# subject-specific emulation

def emu_pwrcal_mux_set_src(uc, *args):
    print('pwrcal_mux_set_src', hex(ARG(uc, 0)), ARG(uc, 1))
    RET(uc, 0)

def emu_grpgate_enable(uc, *args):
    print('grpgate_enable', hex(ARG(uc, 0)))

def error(name, *args):
    raise NotImplementedError(name)
for k in set(y.symbols) | {i[4:] for i in globals() if i.startswith('emu_')}:
    if isinstance(k, str) and k[:1] != '$' and k not in ('memcmp', 'strncmp', 'strstr', 'strlen'):
        x.hook_add(k, globals().get('emu_'+k, error.__get__(k)))

x.create_stack(2**32, 2**20)
x.lr = 1234

with open('ect-dump.bin', 'rb') as file:
    ect_dump = file.read()

x.map_memory_range(0xffffff80f8d00000, 0xffffff80f8d00000+len(ect_dump))
x.uc.mem_write(0xffffff80f8d00000, ect_dump)
x.call('ect_init', 0, len(ect_dump))
x.call('ect_parse_binary_header')
x.call('exynos7870_clk_init', 1234)

# NOTE: offsets obtained through gdb, may be different on your kernel binary

clocks = b''
for a, b, c in clk_providers:
    if b in x.dlsym('of_clk_src_onecell_get'):
        size, addr = divmod(int.from_bytes(x.uc.mem_read(c, 12), 'little'), 2**64)
        clocks += x.uc.mem_read(addr, size*8)

clocks = [int.from_bytes(clocks[i:i+8], 'little', signed=True) for i in range(0, len(clocks), 8)]
clocks = [None if i == -2 else clock_data[i] for i in clocks]
vtables = [int(i[-1][-32:-16], 16) if i is not None and i[0] == 'custom' else None for i in clocks]

vclks = [x.call('cal_get_vclk', int.from_bytes(x.uc.mem_read(i[2]+16, 8), 'little')) if j in x.dlsym('samsung_vclk_ops') else None for i, j in zip(clocks, vtables)]

clk_busy = set()
clk_done = set()
clk_decl = []
clk_print = []
vclk_print = []

@functools.lru_cache(None)
def print_clk(addr):
    if not addr:
        return None if do_json else 'NULL'
    try: name, = (i for i in x.dlsym(addr) if i and '$' not in i)
    except ValueError:
        name = hex(addr)
    if not name.startswith('clk_'):
        name = 'clk_'+name
    idx = int.from_bytes(x.uc.mem_read(addr, 4), 'little')
    if idx == 0:
        return None if do_json else 'CLK_NULL'
    clk_busy.add(addr)
    parent = int.from_bytes(x.uc.mem_read(addr+8, 8), 'little')
    parent_name = print_clk(parent)
    q = []
    qh = []
    for i in (16, 32, 48):
        offset = int.from_bytes(x.uc.mem_read(addr+i, 8), 'little')
        shift = int.from_bytes(x.uc.mem_read(addr+i+8, 2), 'little')
        width = int.from_bytes(x.uc.mem_read(addr+i+10, 2), 'little')
        if offset:
            which, offset = divmod(offset, 2**16)
            reg_addr = offset + int.from_bytes(x.uc.mem_read(next(iter(x.dlsym('v2psfrmap')))+16*which, 8), 'little')
            if do_json:
                q.append({'addr': reg_addr, 'bank': which, 'offset': offset, 'shift': shift, 'width': width})
            else:
                q.append('(%s, %d, %d)'%(hex(reg_addr), shift, width))
            qh.append(True)
        else:
            qh.append(False)
    kind, idx = divmod(idx, 2**24)
    if kind == 1: # fixed_rate
        rate = int.from_bytes(x.uc.mem_read(addr+72, 8), 'little')
        gate = int.from_bytes(x.uc.mem_read(addr+80, 8), 'little')
        assert not q
        if do_json:
            clk_print.append({'type': 'fixed_rate', 'name': name, 'parent': parent_name, 'rate': rate, 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_fixed_rate(%s, %s, %s, %s)'%(name, parent_name, rate, print_clk(gate)))
    elif kind == 2: # fixed_factor
        ratio = int.from_bytes(x.uc.mem_read(addr+72, 2), 'little')
        gate = int.from_bytes(x.uc.mem_read(addr+80, 8), 'little')
        assert not q
        if do_json:
            clk_print.append({'type': 'fixed_div', 'name': name, 'parent': parent_name, 'ratio': ratio, 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_fixed_div(%s, %s, %s, %s)'%(name, parent_name, ratio, print_clk(gate)))
    elif kind == 5: # pll
        assert parent == 0 or int.from_bytes(x.uc.mem_read(parent, 4), 'little') == 0, (parent, parent_name)
        ops = int.from_bytes(x.uc.mem_read(addr+104, 8), 'little')
        if ops in x.dlsym('pll141xx_ops'):
            sfx = '_141xx'
        elif ops in x.dlsym('pll1431x_ops'):
            sfx = '_1431x'
        else:
            assert False
        assert len(q) == 2
        tp = int.from_bytes(x.uc.mem_read(addr+72, 4), 'little')
        rate_table = int.from_bytes(x.uc.mem_read(addr+80, 8), 'little')
        rate_count = int.from_bytes(x.uc.mem_read(addr+88, 4), 'little')
        rates = []
        for i in range(rate_count):
            rate = int.from_bytes(x.uc.mem_read(rate_table+16*i, 8), 'little')
            pdiv = int.from_bytes(x.uc.mem_read(rate_table+16*i+8, 2), 'little')
            mdiv = int.from_bytes(x.uc.mem_read(rate_table+16*i+10, 2), 'little')
            sdiv = int.from_bytes(x.uc.mem_read(rate_table+16*i+12, 2), 'little')
            kdiv = int.from_bytes(x.uc.mem_read(rate_table+16*i+14, 2), 'little')
            if do_json:
                rates.append({'rate': rate, 'pdiv': pdiv, 'mdiv': mdiv, 'sdiv': sdiv, 'kdiv': kdiv})
            else:
                rates.append('{%d, %d, %d, %d, %d}'%(rate, pdiv, mdiv, sdiv, kdiv))
        mux = int.from_bytes(x.uc.mem_read(addr+96, 8), 'little')
        if mux in clk_busy:
            try: mux_name, = (i for i in x.dlsym(mux) if i and '$' not in i)
            except ValueError:
                mux_name = hex(mux)
            if not name.startswith('clk_'):
                mux_name = 'clk_'+mux_name
            if do_json:
                mux = mux_name
            else:
                clk_decl.append('DECLARE_clk_mux(%s)'%mux_name)
                mux = '&'+mux_name
        else:
            mux = print_clk(mux)
        if addr not in clk_done:
            clk_done.add(addr)
            if do_json:
                clk_print.append({'type': 'pll'+sfx, 'name': name, 'main': q[0], 'status': q[1], 'version': tp, 'mux': mux, 'rates': rates})
            else:
                clk_print.append('DEFINE_clk_pll%s(%s, %s, %s, %d, %d, %s, {%s})'%(sfx, name, q[0], q[1], tp, rate_count, mux, ', '.join(rates)))
    elif kind == 6: # mux
        assert parent == 0
        p_parents = int.from_bytes(x.uc.mem_read(addr+72, 8), 'little')
        n_parents = int.from_bytes(x.uc.mem_read(addr+80, 1), 'little')
        gate = int.from_bytes(x.uc.mem_read(addr+88, 8), 'little')
        parents = [print_clk(int.from_bytes(x.uc.mem_read(p_parents+8*i, 8), 'little')) for i in range(n_parents)]
        assert qh[0] and qh[2], qh
        if do_json:
            clk_print.append({'type': 'mux', 'name': name, 'parents': parents, 'main': q[0], 'status': q[1] if len(q) == 3 else None, 'enable': q[-1], 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_mux(%s, (%s), %s, %s, %s, %s)'%(name, ', '.join(parents), q[0], q[1] if len(q) == 3 else '(0, 0, 0)', q[-1], print_clk(gate)))
    elif kind == 7: # div
        assert qh == [True, True, False], qh
        gate = int.from_bytes(x.uc.mem_read(addr+72, 8), 'little')
        if do_json:
            clk_print.append({'type': 'div', 'name': name, 'parent': parent_name, 'main': q[0], 'status': q[1], 'gate': print_clk(gate)})
        else:
            clk_print.append('DEFINE_clk_div(%s, %s, %s, %s, %s)'%(name, parent_name, q[0], q[1], print_clk(gate)))
    elif kind == 8: # gate
        assert qh == [True, False, False], qh
        if do_json:
            clk_print.append({'type': 'gate', 'name': name, 'parent': parent_name, 'main': q[0]})
        else:
            clk_print.append('DEFINE_clk_gate(%s, %s, %s)'%(name, parent_name, q[0]))
    else:
        if do_json:
            clk_print.append({'type': 'unknown', 'name': name, 'parent': parent_name, 'regs': q, 'kind': kind, 'index': idx})
        else:
            clk_print.append('DEFINE_clk_unknown(%s, %s, {%s}, %s, %s)'%(name, parent_name, ', '.join(q), hex(kind), hex(idx)))
    clk_busy.discard(addr)
    return name if do_json else '&'+name

@functools.lru_cache(None)
def print_vclk(addr):
    if not addr:
        return None if do_json else 'NULL'
    try: name, = (i for i in x.dlsym(addr) if i and '$' not in i)
    except ValueError:
        name = hex(addr)
    if not name.startswith('vclk_'):
        name = 'vclk_'+name
    parent = int.from_bytes(x.uc.mem_read(addr+8, 8), 'little')
    parent_name = print_vclk(parent)
    vtable = int.from_bytes(x.uc.mem_read(addr+40, 8), 'little')
    if not vtable:
        return None if do_json else 'VCLK_NULL'
    try: vtable_name, = (i for i in x.dlsym(vtable) if '$' not in i)
    except ValueError: vtable_name = hex(vtable)
    if vtable_name == 'grpgate_ops':
        gatep = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        gates = []
        while True:
            gate = int.from_bytes(x.uc.mem_read(gatep, 8), 'little')
            gatep += 8
            if gate in x.dlsym('clk_0'):
                break
            gates.append(print_clk(gate))
        if do_json:
            vclk_print.append({'type': 'grpgate', 'name': name, 'parent': parent_name, 'gates': gates})
        else:
            vclk_print.append('DEFINE_grpgate(%s, %s, (%s))'%(name, parent_name, ', '.join(i for i in gates)))
    elif vtable_name == 'pxmxdx_ops':
        plist = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        children = []
        while True:
            clk = int.from_bytes(x.uc.mem_read(plist, 8), 'little')
            if clk in x.dlsym('clk_0'):
                break
            config0 = int.from_bytes(x.uc.mem_read(plist+8, 4), 'little', signed=True)
            config1 = int.from_bytes(x.uc.mem_read(plist+12, 4), 'little', signed=True)
            plist += 16
            children.append('{%s, %d, %d}'%(print_clk(clk), config0, config1))
        if do_json:
            vclk_print.append({'type': 'pxmxdx', 'name': name, 'parent': parent_name, 'children': children})
        else:
            vclk_print.append('DEFINE_pxmxdx(%s, %s, {%s})'%(name, parent_name, ', '.join(children)))
    elif vtable_name == 'd1_ops':
        div = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        if do_json:
            vclk_print.append({'type': 'd1', 'name': name, 'parent': parent_name, 'clk': print_clk(div)})
        else:
            vclk_print.append('DEFINE_d1(%s, %s, %s)'%(name, parent_name, print_clk(div)))
    elif vtable_name == 'p1_ops':
        pll = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        if do_json:
            vclk_print.append({'type': 'p1', 'name': name, 'parent': parent_name, 'clk': print_clk(pll)})
        else:
            vclk_print.append('DEFINE_p1(%s, %s, %s)'%(name, parent_name, print_clk(pll)))
    elif vtable_name == 'umux_ops':
        umux = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        if do_json:
            vclk_print.append({'type': 'umux', 'name': name, 'parent': parent_name, 'clk': print_clk(umux)})
        else:
            vclk_print.append('DEFINE_umux(%s, %s, %s)'%(name, parent_name, print_clk(umux)))
    elif vtable_name == 'm1d1g1_ops':
        mux = int.from_bytes(x.uc.mem_read(addr+48, 8), 'little')
        div = int.from_bytes(x.uc.mem_read(addr+56, 8), 'little')
        gate = int.from_bytes(x.uc.mem_read(addr+64, 8), 'little')
        extra_mux = int.from_bytes(x.uc.mem_read(addr+72, 8), 'little')
        if do_json:
            vclk_print.append({'type': 'm1d1g1', 'name': name, 'parent': parent_name, 'mux': print_clk(mux), 'div': print_clk(div), 'gate': print_clk(gate), 'extmux': print_clk(extra_mux)})
        else:
            vclk_print.append('DEFINE_m1d1g1(%s, %s, %s, %s, %s, %s)'%(name, parent_name, print_clk(mux), print_clk(div), print_clk(gate), print_clk(extra_mux)))
    elif vtable_name == 'dfs_ops':
        if do_json:
            vclk_print.append({'type': 'unimplemented', 'name': name})
        else:
            return 'VCLK_NYI' # NYI
    else:
        if do_json:
            vclk_print.append({'type': 'unknown', 'name': name, 'parent': parent_name, 'vtable': vtable_name})
        else:
            vclk_print.append('DEFINE_unknown(%s, %s, %s)'%(name, parent_name, vtable_name))
    return name if do_json else '&'+name

do_json = False

if '--json' in sys.argv:
    do_json = True
    del sys.argv[sys.argv.index('--json')]

vclk_names = [print_vclk(i) if i is not None else None for i in vclks]

with open(sys.argv[1], 'w') as output:
    if do_json:
        json.dump({
            'clocks': clk_print,
            'virtual_clocks': vclk_print,
            'vclk_table': {k: v for k, v in enumerate(vclk_names) if v is not None},
        }, output, indent=4)
        output.write('\n')
    else:
        print('\n/* CLOCKS */\n', file=output)
        for i in clk_decl:
            print(i, file=output)
        for i in clk_print:
            print(i, file=output)
        print('\n/* VIRTUAL CLOCKS */\n', file=output)
        for i in vclk_print:
            print(i, file=output)
        prev = -1
        print('\nVCLK_TABLE({', file=output)
        for i, j in enumerate(vclk_names):
            if j is not None:
                if i == prev + 1:
                    print('    %s,'%j, file=output)
                else:
                    print('    [%d] = %s,'%(i, j), file=output)
                prev = i
        print('})', file=output)
